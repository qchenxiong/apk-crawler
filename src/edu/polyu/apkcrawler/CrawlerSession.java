/* 
 * Copyright (c) 2011 Raunak Gupta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.polyu.apkcrawler;

import java.util.Locale;

import com.gc.android.market.api.MarketSession;

import edu.polyu.apkcrawler.util.Device;
import edu.polyu.apkcrawler.util.User;

/**
 * A <code>Session</code> object can be seen as a virtual Android Device. It
 * stores information on the user, the device they using (device name + android
 * version), and the current state of session - whether its being used or not.
 * 
 * When a <code>MarketSession</code> (a session which allows data to be
 * transferred from the marketplace) is expired, the <code>Session</code> class
 * is capable creating a new one.
 * 
 * @author raunak
 * @version 1.0
 */
public class CrawlerSession {

	/**
	 * A <code>User</code> object, holding information on a real user who wishes
	 * to access the Google Android Marketplace using their Google username
	 */
	private User user;

	/**
	 * A <code>Device</code> object holding information on a virtual Android
	 * device.
	 */
	private Device device;

	/**
	 * a boolean flag. set to true if the session is corrupted; false otherwise
	 */
	private boolean stale;

	/**
	 * <code>MarketSession</code> is a gateway between the crawler and the
	 * Android Marketplace. A valid marketSession object allows one to pull
	 * information from the Android Marketplace.
	 */
	private MarketSession marketSession;

	/**
	 * Constructs a <code>Session</code> using the passed parameters
	 * 
	 * @param device
	 *            the device associated with the <code>Session</code>. See
	 *            <code>DeviceInventory.java</code>
	 * @param user
	 *            the user associated with the <code>Session</code>.
	 */
	public CrawlerSession(Device device, User user) {
		this.stale = false;
		this.user = user;
		this.device = device;

		/*
		 * http://code.google.com/p/android-market-api/
		 * use the Market API to login, get an authToken
		 */
		this.marketSession = new MarketSession();
		
//		this.marketSession.setLocale(new Locale("ja", "JP"));
		this.marketSession.setLocale(new Locale("en", "US"));
//		this.marketSession.setOperator("T-Mobile", "31026");
		
//		this.marketSession.setLocale(new Locale("zh", "CN"));
//		this.marketSession.setLocale(new Locale("zh", "HK"));	// 等了一会，但还是failed
//		this.marketSession.setOperator("CMCC HK", "45412");
		
		// login过程中仅仅需要AndroidId
		this.marketSession.getContext().setAndroidId(device.getAndroidId());
		this.marketSession.getContext().setDeviceAndSdkVersion(device.toString());
		// 已经先登录了啊!!!
		this.marketSession.login(user.getUsername(), user.getPassword());
		// TODO: dao
		this.marketSession.loginHttps(user.getUsername(), user.getPassword());
	}

	/**
	 * Gets the validity of <code>Session</code> object
	 * 
	 * @return stale; a boolean
	 */
	public boolean isStale() {
		return stale;
	}

	/**
	 * Set the validity of <code>Session</code> object
	 * 
	 * @param stale
	 *            current state of <code>Sesssion</code>
	 */
	public void setStale(boolean stale) {
		this.stale = stale;
	}

	/**
	 * Gets the <code>User</code> object associated with this
	 * <code>Session</code
	 * 
	 * @return user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Gets the <code>Device</code> object associated with this
	 * <code>Session</code>
	 * 
	 * @return device
	 */
	public Device getDevice() {
		return device;
	}

	/**
	 * Gets the <code>MarketSession</code> object associated with this
	 * <code>CrawlerSession</code>
	 * 
	 * @return marketSession
	 */
	public MarketSession getMarketSession() {
		return marketSession;
	}
	
	/**
	 * Gets the AuthSubToken associated with this
	 * <code>CrawlerSession</code>
	 * 
	 * @return marketSession.getAuthSubToken()
	 */
	public String getAuthToken() {
		return marketSession.getAuthSubToken();
	}
	
	/**
     * Gets the Cookie associated with this
     * <code>CrawlerSession</code>
     * 
     * @return "ANDROID="+this.getAuthToken()
     * @author daoyuan
     */
    public String getCookie() {
        return "ANDROID="+this.getAuthToken();
    }
	
	/**
	 * Gets the SecureToken associated with this
     * <code>CrawlerSession</code>
     * 
     * @return marketSession.getAuthSubToken()
     * @author daoyuan
	 */
    public String getSecureToken() {
        return marketSession.getSecureToken();
    }

	/**
	 * Sets the <code>MarketSession</code> object associated with this
	 * <code>Session</code>
	 * 
	 * @param marketSession
	 *            a valid <code>MarketSession</code> object which can be used to
	 *            access Google Android Marketplace
	 */
	public void setMarketSession(MarketSession marketSession) {
		this.stale = false;
		this.marketSession = marketSession;
	}

	/**
	 * @deprecated
	 * Refreshes the <code>MarketSession</code> object
	 */
	public void refreshMarketSession() {
		this.stale = false;

		this.marketSession = new MarketSession();
		this.marketSession.login(user.getUsername(), user.getPassword());
		this.marketSession.getContext().setAndroidId(device.getAndroidId());
		this.marketSession.getContext().setDeviceAndSdkVersion(device.toString());
	}
}




