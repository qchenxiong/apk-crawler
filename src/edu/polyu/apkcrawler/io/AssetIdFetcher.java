/* 
 * Copyright (c) 2011 Raunak Gupta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.polyu.apkcrawler.io;

import com.gc.android.market.api.MarketSession;
import com.gc.android.market.api.MarketSession.Callback;
import com.gc.android.market.api.model.Market;
import com.gc.android.market.api.model.Market.AppsRequest;
import com.gc.android.market.api.model.Market.AppsRequest.Builder;
import com.gc.android.market.api.model.Market.AppsRequest.OrderType;
import com.gc.android.market.api.model.Market.AppsRequest.ViewType;
import com.gc.android.market.api.model.Market.AppsResponse;
import com.gc.android.market.api.model.Market.ResponseContext;

/**
 * <code>Fetcher</code> fetches the app info (app details, comments, and images
 * ) from the Marketplace.
 * 
 * @author raunak
 * @version 1.0
 */
public class AssetIdFetcher {
	
	private volatile AppsResponse appResponse;

	/**
	 * Get App(s) on Android Marketplace by Category
	 * 
	 * @param marketSession
	 *            the <code>MarketSession</code> object to use to fetch the
	 *            app(s) for the app
	 * @param categoryName
	 *            the name of category to fetch app(s) via
	 * @param startIndex
	 *            the index from where to begin fetching app(s) from
	 *            the ranking
	 * @param orderTypeValue
	 * 	  case 0: return NONE;
          case 1: return POPULAR;
          case 2: return NEWEST;
          case 3: return FEATURED;
     * @param viewTypeValue
     * 	  case 0: return ALL;
          case 1: return FREE;
          case 2: return PAID;
	 * 
	 * @return AppsResponse object containing information on App(s).
	 */
	synchronized public AppsResponse getAppsByCategory(
			MarketSession marketSession, String categoryName,
			int startIndex, int orderTypeValue, int viewTypeValue) {
		this.appResponse = null;

		Builder builder = AppsRequest.newBuilder()
				.setCategoryId(categoryName)
				.setStartIndex(startIndex)
				.setEntriesCount(10)		// 要跟startIndex += 10;对应
				.setWithExtendedInfo(true)
				.setViewType( ViewType.valueOf(viewTypeValue) )
				.setOrderType( OrderType.valueOf(orderTypeValue) );
		AppsRequest request = builder.build();

		marketSession.append(request, new Callback<Market.AppsResponse>() {
			public void onResult(ResponseContext context, AppsResponse response) {
				
				setAppsResponse(response);
			}
		});

		marketSession.flush();
		return getAppsResponse();
	}

	/**
	 * Get app from the Marketplace via package name.
	 * 
	 * @param marketSession
	 *            the <code>MarketSession</code> object to use to fetch the app
	 * @param packageName
	 *            the package to fetch from the marketplace
	 * @return AppResponse object containing information on App(s).
	 */
	synchronized public AppsResponse getAppsByPackageName(
	        MarketSession marketSession,
	        int entriesCount,
	        String packageName ) {
		this.appResponse = null;
		
		// 这边有点不懂??
		AppsRequest request = AppsRequest.newBuilder()
				.setQuery("pname:" + "\"" + packageName + "\"")
				.setStartIndex(0)
				.setEntriesCount(entriesCount)	// 感觉1个就行啊
				.setViewType( ViewType.valueOf(0) )		// case 0: return ALL;
				.build();

		marketSession.append( request, new Callback<Market.AppsResponse>() {		
			public void onResult(ResponseContext context, AppsResponse response) {
				setAppsResponse(response);
			}		
		});

		marketSession.flush();
		return getAppsResponse();
	}
	
	/**
	 * 
	 * @param marketSession
	 * @param pubName
	 * @param onePubIndex
	 * @param viewTypeValue    先只爬免费的吧
     *    case 0: return ALL;
          case 1: return FREE;
          case 2: return PAID;
	 * @return
	 */
    synchronized public AppsResponse getAppsByPublisher(
                    MarketSession marketSession,
                    String pubName,
                    int onePubIndex,
                    int entriesCount,
                    int viewTypeValue) {
        this.appResponse = null;
        
        AppsRequest request = AppsRequest.newBuilder()
                .setQuery("pub:" + "\"" + pubName + "\"")
                .setStartIndex(onePubIndex)
                .setEntriesCount(entriesCount)
                .setWithExtendedInfo(true)
                // TODO: 会不会不设就可以爬PAID的??
                .setViewType( ViewType.valueOf(viewTypeValue) )
                .build();

        marketSession.append( request, new Callback<Market.AppsResponse>() {        
            public void onResult(ResponseContext context, AppsResponse response) {
            	// 这边才会给它赋值
                setAppsResponse(response);
            }
        });

        marketSession.flush();
        return getAppsResponse();
    }
    
    /**
     * 
     * @param marketSession
     * @param queryWord
     * @param startIndex
     * @param viewTypeValue 爬收费的会getassetResponse is null
     *    case 0: return ALL;
          case 1: return FREE;
          case 2: return PAID;
     * @return
     */
    synchronized public AppsResponse getAppsByQueryWord(
		            MarketSession marketSession,
		            String queryWord,
		            int startIndex,
		            int viewTypeValue) {
		this.appResponse = null;
		
		AppsRequest request = AppsRequest.newBuilder()
		        .setQuery(queryWord)
		        .setStartIndex(startIndex)
		        .setEntriesCount(10)
		        .setWithExtendedInfo(true)
		        // TODO: 会不会不设就可以爬PAID的??
		        .setViewType( ViewType.valueOf(viewTypeValue) )
		        .build();
		
		marketSession.append( request, new Callback<Market.AppsResponse>() {        
		    public void onResult(ResponseContext context, AppsResponse response) {
		        setAppsResponse(response);
		    }
		});
		
		marketSession.flush();
		return getAppsResponse();
	}

	private void setAppsResponse(AppsResponse response) {
		this.appResponse = response;
	}

	private AppsResponse getAppsResponse() {
		return appResponse;
	}

}






