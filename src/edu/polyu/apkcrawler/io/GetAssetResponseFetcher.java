/*
 * TODO: dao
 */
package edu.polyu.apkcrawler.io;

import com.gc.android.market.api.MarketSession;
import com.gc.android.market.api.MarketSession.Callback;
import com.gc.android.market.api.model.Market;
import com.gc.android.market.api.model.Market.GetAssetRequest;
import com.gc.android.market.api.model.Market.GetAssetResponse;
import com.gc.android.market.api.model.Market.ResponseContext;

public class GetAssetResponseFetcher {
    
    private volatile GetAssetResponse getassetResponse;
    
    synchronized public GetAssetResponse getGetAssetResponseById(
            MarketSession marketSession, String assetId) {
        this.getassetResponse = null;
        
        boolean oldIsSecure = marketSession.getContext().getIsSecure();
        marketSession.getContext().setIsSecure(true);
        // set secure token as authSubToken
        // all operation in context
        String oldauthSubToken = marketSession.getContext().getAuthSubToken();
        marketSession.getContext().setAuthSubToken(marketSession.getSecureToken());
        
        // construct request
        GetAssetRequest request = GetAssetRequest.newBuilder()
            .setAssetId(assetId)
            .build();
        
        // get response
        marketSession.append(request, new Callback<Market.GetAssetResponse>() {        
            public void onResult(ResponseContext context, GetAssetResponse response) {
                setGetAssetResponse(response);
            }
        });
        // TODO: dao
        marketSession.flushHttps();
        
        marketSession.getContext().setIsSecure(oldIsSecure);
        marketSession.getContext().setAuthSubToken(oldauthSubToken);
        return getGetAssetResponse();
    }
            
    
    private void setGetAssetResponse(GetAssetResponse response) {
        this.getassetResponse = response;
    }
    
    private GetAssetResponse getGetAssetResponse() {
        return getassetResponse;
    }
}
