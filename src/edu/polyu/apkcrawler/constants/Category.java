/* 
 * Copyright (c) 2011 Raunak Gupta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.polyu.apkcrawler.constants;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A list of App categories that the Android Marketplace supports.
 * 
 * @author raunak
 * @version 1.0
 * 
 */
public class Category {
	/**
	 * 1_ARCADE: "ARCADE"
	 * 并没有改
	 */
	public static final String ARCADE = "ARCADE";
	
	/**
	 * 2_DEMO: "LIBRARIES_AND_DEMO"
	 */
	public static final String DEMO = "LIBRARIES_AND_DEMO";
	
	/**
	 * 3_ENTERTAINMENT: "ENTERTAINMENT"
	 */
	public static final String ENTERTAINMENT = "ENTERTAINMENT";
	
	/**
	 * 4_FINANCE: "FINANCE"
	 */
	public static final String FINANCE = "FINANCE";
	
	/**
	 * 5_HEALTH: "HEALTH_AND_FITNESS"
	 */
	public static final String HEALTH = "HEALTH_AND_FITNESS";
	
	/**
	 * 6_LIBRARIES: "LIBRARIES_AND_DEMO"
	 * 跟2一样的，舍弃
	 */
	public static final String LIBRARIES = "LIBRARIES_AND_DEMO";
	
	/**
	 * 7_LIFESTYLE: "LIFESTYLE"
	 */
	public static final String LIFESTYLE = "LIFESTYLE";

	/**
	 * 8_MULTIMEDIA: "MEDIA_AND_VIDEO"
	 */
	public static final String MULTIMEDIA = "MEDIA_AND_VIDEO";

	/**
	 * 9_NEWS: "NEWS_AND_MAGAZINES"
	 */
	public static final String NEWS = "NEWS_AND_MAGAZINES";
	
	/**
	 * 10_REFERENCE: "BOOKS_AND_REFERENCE"
	 */
	public static final String REFERENCE = "BOOKS_AND_REFERENCE";
	
	/**
	 * 11_THEMES: "THEMES"已经不存在了
	 * --> "PERSONALIZATION"
	 * 跟24一样，舍弃
	 */
	public static final String THEMES = "PERSONALIZATION";
	
	/**
	 * 12_TRAVEL: "TRAVEL_AND_LOCAL"
	 */
	public static final String TRAVEL = "TRAVEL_AND_LOCAL";
	
	/**
	 * 13_BRAIN: "BRAIN"
	 * 并没有改
	 */
	public static final String BRAIN = "BRAIN";
	
	/**
	 * 14_CARDS: "CARDS"
	 * 并没有改
	 */
	public static final String CARDS = "CARDS";
	
	/**
	 * 15_CASUAL: "CASUAL"
	 */
	public static final String CASUAL = "CASUAL";
	
	/**
	 * 16_COMICS: "COMICS"
	 */
	public static final String COMICS = "COMICS";
	
	/**
	 * 17_COMMUNICATION: "COMMUNICATION"
	 */
	public static final String COMMUNICATION = "COMMUNICATION";

	/**
	 * 18_PRODUCTIVITY: "PRODUCTIVITY"
	 */
	public static final String PRODUCTIVITY = "PRODUCTIVITY";

	/**
	 * 19_SHOPPING: "SHOPPING"
	 */
	public static final String SHOPPING = "SHOPPING";

	/**
	 * 20_SOCIAL: "SOCIAL"
	 */
	public static final String SOCIAL = "SOCIAL";

	/**
	 * 21_SPORTS: "SPORTS"
	 */
	public static final String SPORTS = "SPORTS";
	
	/**
	 * 22_TOOLS: "TOOLS"
	 */
	public static final String TOOLS = "TOOLS";	
	
	/**
     * 23_BUSINESS: "BUSINESS"
     */
    public static final String BUSINESS = "BUSINESS";
    
    /**
     * 24_PERSONALIZATION: "PERSONALIZATION"
     */
    public static final String PERSONALIZATION = "PERSONALIZATION";
    
    /**
     * 25_APP_WALLPAPER: "APP_WALLPAPER"
     */
    public static final String APP_WALLPAPER = "APP_WALLPAPER";
    
    /**
     * 26_SPORTS_GAMES: "SPORTS_GAMES"
     */
    public static final String SPORTS_GAMES = "SPORTS_GAMES";
    
    /**
     * 27_GAME_WALLPAPER: "GAME_WALLPAPER"
     */
    public static final String GAME_WALLPAPER = "GAME_WALLPAPER";
    
    /**
     * 28_RACING: "RACING"
     */
    public static final String RACING = "RACING";
    
    /**
     * 29_GAME_WIDGETS: "GAME_WIDGETS"
     */
    public static final String GAME_WIDGETS = "GAME_WIDGETS";
    
    /**
     * 30_APP_WIDGETS: "APP_WIDGETS"
     */
    public static final String APP_WIDGETS = "APP_WIDGETS";
    
    /**
     * 31_EDUCATION: "EDUCATION"
     */
    public static final String EDUCATION = "EDUCATION";
    
    /**
     * 32_MEDICAL: "MEDICAL"
     */
    public static final String MEDICAL = "MEDICAL";
    
    /**
     * 33_MUSIC_AND_AUDIO: "MUSIC_AND_AUDIO"
     */
    public static final String MUSIC_AND_AUDIO = "MUSIC_AND_AUDIO";
    
    /**
     * 34_PHOTOGRAPHY: "PHOTOGRAPHY"
     */
    public static final String PHOTOGRAPHY = "PHOTOGRAPHY";
    
    /**
     * 35_TRANSPORTATION: "TRANSPORTATION"
     */
    public static final String TRANSPORTATION = "TRANSPORTATION";
    
    /**
     * 36_WEATHER: "WEATHER"
     */
    public static final String WEATHER = "WEATHER";
    

	/**
	 * Gets all the categories that are available
	 * 
	 * @return <code>Iterator</code><<code>String</code>>
	 */
	public static Iterator<String> getAllCategories() {
		List<String> categories = new ArrayList<String>(22);

		categories.add(ARCADE);
		categories.add(DEMO);
		categories.add(ENTERTAINMENT);
		categories.add(FINANCE);
		categories.add(HEALTH);
		categories.add(LIBRARIES);
		categories.add(LIFESTYLE);
		categories.add(MULTIMEDIA);
		categories.add(NEWS);
		categories.add(REFERENCE);
		categories.add(THEMES);
		categories.add(TRAVEL);
		categories.add(BRAIN);
		categories.add(CARDS);
		categories.add(CASUAL);
		categories.add(COMICS);
		categories.add(COMMUNICATION);
		categories.add(PRODUCTIVITY);
		categories.add(SHOPPING);
		categories.add(SOCIAL);
		categories.add(SPORTS);
		categories.add(TOOLS);
		/**
		 * @author daoyuan
		 * 23 ~ 36
		 */
		categories.add(BUSINESS);
		categories.add(PERSONALIZATION);
		categories.add(APP_WALLPAPER);
		categories.add(SPORTS_GAMES);
		categories.add(GAME_WALLPAPER);
		categories.add(RACING);
		categories.add(GAME_WIDGETS);
		categories.add(APP_WIDGETS);
		categories.add(EDUCATION);
		categories.add(MEDICAL);
		categories.add(MUSIC_AND_AUDIO);
		categories.add(PHOTOGRAPHY);
		categories.add(TRANSPORTATION);
		categories.add(WEATHER);

		return categories.iterator();
	}
}
