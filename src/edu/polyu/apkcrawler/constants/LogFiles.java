package edu.polyu.apkcrawler.constants;

public class LogFiles {
	public static String crawlerLog = "_CRAWLERLOG";
	
	public static String totalIndex = "_TOTALINDEX";
	
	public static String errorApks  = "_ERRORAPKS";
	
	public static String ratingLog  = "_RATINGLOG";
	
	public static String errorPubs  = "_ERRORPUBS";
}