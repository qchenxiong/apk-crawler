package edu.polyu.apkcrawler.constants;

/**
 * 
 * @author dao
 * @since 1.1-13
 */
public class Configuration {
    
    /**
     * 120s
     */
    public static final int SOCKTIMEOUT = 120000;

}
