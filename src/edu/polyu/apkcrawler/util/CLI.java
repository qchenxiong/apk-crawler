package edu.polyu.apkcrawler.util;

import java.util.Iterator;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import edu.polyu.apkcrawler.constants.Category;

/**
 * A simple Command Line Interface options manager.
 * 
 * @author raunak
 * @version 1.0
 */
public class CLI {

	/**
	 * Holds many <code>Option</code> objects
	 */
	private Options options = new Options();
	
	private String allCategories = "";

	/**
	 * Constructs a <code>CLI</code> object
	 */
	public CLI() {
		Iterator<String> categories = Category.getAllCategories();
		int i = 0;
		
		while ( categories.hasNext() ) {
			i++;
			allCategories += (i + ": " + categories.next() + "\n");
		}
		
		/**
		 * new
		 */
		// general must
//		addOption(new Option("ue", "useremail", true, "your gmail address"));
//		addOption(new Option("up", "userpwd", true, "your gmail password"));
		// general maybe
		addOption(new Option("ti", "totalIndex", true, "total index for several option"));
		addOption(new Option("tc", "totalCount", true, "total count for category <= 450/480, for pkg query, for pub query"));
		addOption(new Option("st", "smallTime", true, "sleep for a smaltime during each request"));
		addOption(new Option("oai", "oneAccountIndex", true, "which account will be used for loginOneSession()"));
		// general for "bc", "qw"
		addOption(new Option("si", "startIndex", true, "start index for this category or queryword"));
		addOption(new Option("ot", "orderType", true, "order type value for this category or queryword.\n1: POPULAR, 2: NEWEST, 3: FEATURED"));
		// TODO 参数指定哪个category
		// by input
		addOption(new Option("pkg", "package", true, "fetch one app by package name"));	// 参数指定哪个category
		addOption(new Option("pkf", "pkgfile", true, "fetch apps by package file which specify category"));
		addOption(new Option("puf", "pubfile", true, "fetch apps by publisher file"));
		addOption(new Option("qw", "queryword", true, "fetch apps by query word"));
		addOption(new Option("qc", "querycount", true, "how many apps fetched by by query keyword, default is 24 * 8 = 192"));
		addOption(new Option("bc", "bycategory", true, "fetch apps by this category(from 1 to 36, without 6 and 11)"));
		addOption(new Option("ac", "allcategory", false, "fetch apps from all categories(from 1 to 36, without 6 and 11)"));
		addOption(new Option("sac", "smallallcategory", true, "small index for allcategory(from 1 to 36, without 6 and 11)"));
		// by sql file: order, category
		addOption(new Option("sqlpkf", "sqlpkgfile", true, "fetch many apps by pkg in sql file"));
		addOption(new Option("sqlpuf", "sqlpubfile", true, "fetch many apps by pub in sql file"));
		
		/**
		 * old
		 */
//		addOption(new Option("ue", "useremail", true, "your gmail address"));
//		addOption(new Option("up", "userpwd", true, "your gmail password"));
//		addOption(new Option("c", "category", true, "fetch apps for this category\n" + allCategories));
//		addOption(new Option("s", "startIndex", true, "start index for this category"));
//		addOption(new Option("ti", "totalIndex", true, "total index for several option"));
//		addOption(new Option("tc", "totalCount", true, "total count for this category"));
//		addOption(new Option("p", "package", true, "fetch one app by package name"));
//		addOption(new Option("pkf", "pkgfile", true, "fetch apps by package file"));
//		addOption(new Option("puf", "pubfile", true, "fetch apps by publisher file"));
//		addOption(new Option("st", "smalltime", true, "sleep for a smaltime during each request"));
//		addOption(new Option("qw", "queryword", true, "fetch apps by query word"));
//		addOption(new Option("qc", "querycount", true, "how many apps fetched by by query keyword, default is 24 * 8 = 192"));
	}

	/**
	 * Adds an <code>Option</code> to a list.
	 * 
	 * @param option
	 *            the <code>Option</code> or the argument that you wish to add.
	 */
	private void addOption(Option option) {
		this.options.addOption(option);
	}

	/**
	 * Get the available options for command line interface
	 * 
	 * @return list of supported arg(s)
	 */
	public Options getOptions() {
		return options;
	}
}
