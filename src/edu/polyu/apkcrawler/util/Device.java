/* 
 * Copyright (c) 2011 Raunak Gupta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
// Modified by Daoyuan WU
package edu.polyu.apkcrawler.util;

/**
 * <code>Device</code> holds information about a Virtual Android device.
 * 
 * @author raunak
 * @version 1.0
 * 
 */
public class Device {

	/**
	 * The name of device
	 */
	private String deviceName;

	/**
	 * The version of Android the device is running.
	 */
	private int deviceVersion;
	
	/**
	 * 通常用做识别唯一设备
	 */
	private String deviceId;
	
	/**
	 * deviceId跟ANDROID_ID有一定的关系，deviceId是Google服务器自动生成的
	 */
	private String androidId;

	/**
	 * Constructs a <code>Device</code> using the passed parameters.
	 * 
	 * @param deviceVersion
	 *            The version of Android the device is running.
	 * @param deviceName
	 *            The device name.
	 */
	public Device(int deviceVersion, String deviceName, String deviceId, String androidId) {
		this.deviceName = deviceName;
		this.deviceVersion = deviceVersion;
		this.deviceId = deviceId;
		this.androidId = androidId;
	}

	/**
	 * Gets the name of Android device.
	 * 
	 * @return deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * Gets the version of android running on device.
	 * 
	 * @return deviceVersion
	 */
	public int getDeviceVersion() {
		return deviceVersion;
	}
	
	/**
	 * Gets the deviceID for device.
	 * 
	 * @return deviceID
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * Gets the Android_ID for device.
	 * 
	 * @return androidID
	 */
	public String getAndroidId() {
		return androidId;
	}

	/**
	 * Set the deviceID for device
	 * 
	 * @param deviceID
	 */
	public void setDeviceId( String deviceID ) {
		this.deviceId = deviceID;
	}

	@Override
	public String toString() {
		return deviceName + ":" + deviceVersion;
	}
}
